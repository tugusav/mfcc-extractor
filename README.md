# MFCC Extractor

Mel Frequency Cepstral Coefficients Extraction API using Python (Librosa, Scipy)

## Description

This is a REST API built using FastAPI library on Python to extract Mel Frequency Cepstral Coefficients (MFCC) from an audio file, typically a .wav file. This API will need some parameters such as frame length, frame shift, number of Mel Filter Banks and also an audio file.

Using a POST request, users can use this API and receive a matrix of N X 13, where N is the number of frames of the audio and 13 is the number of cepstral coefficients returned.

## Running the API

This API is built on top of Python, so what you'll need is Python v.3.8 and above

### Dependencies 

The `requirements.txt` file contains all the dependencies needed to run this API. You can install all the dependencies using pip inside the main directory `mfcc-extractor`.

```
pip install -r requirements.txt
```
The main libraries used in this API are as follow
- librosa==0.9.1
- scipy==1.8.0
- numpy==1.20.0
- uvicorn==0.17.6
- python-multipart==0.0.5
- fastapi==0.77.1

If you encountered error while installing using `pip install -r requirements.txt` like this:
```
ImportError: Numba needs NumPy 1.21 or less
```
Try fixing it by calling `pip install numpy==1.20.0` to install a earlier version of numpy.


## Running the API
To run the API, first go to the main directory `mfcc-extractor` and run the `main.py` file using this command

```
uvicorn main:app --reload
```
The API server will run on http://127.0.0.1:8000

```
/mfcc-extractor> uvicorn main:app --reload
INFO:     Will watch for changes in these directories: [\mfcc-extractor']
INFO:     Uvicorn running on http://127.0.0.1:8000 (Press CTRL+C to quit)
INFO:     Started reloader process [22152] using statreload
WARNING:  The --reload flag should not be used in production on Windows.
INFO:     Started server process [15504]
INFO:     Waiting for application startup.
INFO:     Application startup complete.
```

## Access the API Endpoints

FastAPI already have a GUI to easily try and access the API Endpoints. You can access the API endpoint through http://127.0.0.1:8000/docs#/

![API GUI](/img/docs.png?raw=true "The GUI of FastAPI")

The API endpoint is http://127.0.0.1:8000/mfcc_extractor

### Parameters
The parameters needed in this API endpoint is
- audio_file: An audio file (typically a .wav file)
- frame_length: The length of audio frames to be windowed (in milliseconds, default 25ms)
- frame_shift: The length of overlapped audio in each frames (in milliseconds, default 10ms)
- num_mel_bins: The number of mel filters to be applied on the audio power spectogram (an integer number, default 26)

### Using the API
- Press the 'Try it out' button on the API method
- Insert the parameters mentioned above. The parameters are already filled with default values
- Upload the file
- Press 'Execute' and take a look at the response body

A successful POST request would look like this
![Response](/img/request.png?raw=true "Successful Request")

### Response

The response of the API is in JSON format (content-type header 'application/json') as follows

```
{
    filename: "your_audio_file_name",
    "mfcc": "N X 13 Matrix"
}
```

## Author
Ida Bagus Raditya Avanindra Mahaputra\
18219117\
Sistem dan Teknologi Informasi\
Institut Teknologi Bandung
