# Author : Ida Bagus Raditya Avanindra Mahaputra
# Date: 12 May 2022

import librosa
import numpy as np
import scipy
from fastapi import Depends, FastAPI, HTTPException, UploadFile, File
import json

app = FastAPI(
    title='MFCC Extractor',
    description='MFCC Extractor API helps you to extract 13 Cepstral Coefficients of an audio file, by defining the frame length, frame shift and number of mel filters',
    contact={
        'name': 'Ida Bagus Raditya Avanindra Mahaputra',
        'email': 'tugus.ibram@gmail.com'
    }

)
# Function to load the audio file

def loadAudio(file):
    signal, sample_rate = librosa.load(file.file, sr=16000) # we want the audio to be resampled as 16KHz
    return signal, sample_rate

# Function for windowing, fourier transform, and getting the spectogram
def spectogram(signal, sample_rate, frame_length=0.025, frame_shift=0.01):
    # using stft to compute dft using windowing
    nfft = int(frame_length*sample_rate) # standard is 400 for 16KHz and 25ms window
    hop = int(frame_shift*sample_rate) # standard is 160 for 16KHz and 10ms overlap
    
    # applying short time fourier transform the signal
    stft = librosa.stft(signal, n_fft=nfft, hop_length=hop, window='hamming')
    
    # computing the power spectogram
    power_spectogram = np.abs(stft) ** 2
    return power_spectogram

# Function for extracting mel power spectogram

def mel_power_spectogram(power_spectogram, sample_rate, num_mel_bins=26):
    # applying melspectogram from power_spectogram
    mel_power_spectogram = librosa.feature.melspectrogram(S=power_spectogram, sr=sample_rate, n_mels=num_mel_bins)
    # return the log of mel power spectogram
    return mel_power_spectogram

# Function to return log of the mel filtered power spectrum
def log_mel_spectrogram(mel_power_spectogram):
    return np.log10(mel_power_spectogram)

# DCT function to return 13 mel filtered cepstral coefficients
def dct(mel_power_spectogram):
    return scipy.fftpack.dct(mel_power_spectogram, type=3)[:13]


def mfcc(path, frame_length=0.025, frame_shift=0.01, num_mel_bins=26):
    # load audio file
    signal, sample_rate = loadAudio(path)
    
    # windowing and applying dft to signal
    power_spectogram = spectogram(signal, sample_rate, frame_length, frame_shift)

    # applying mel filterbank to power spectogram
    mel_spectogram = mel_power_spectogram(power_spectogram, sample_rate, num_mel_bins)

    # applying log to mel power spectogram
    log_spectogram = np.log(mel_spectogram)

    # applying dct to log mel spectrogram
    mfcc = dct(log_spectogram)
    
    return mfcc


# API Endpoints
@app.post("/mfcc_extractor", tags=['Extract MFCC'])
def mfcc_extractor(audio_file: UploadFile = File(...), frame_length: float =0.025, frame_shift: float =0.01, num_mel_bins: int =26):
    # checking if audio file is empty
    if audio_file.filename is None:
        raise HTTPException(status_code=400, detail="No file uploaded")
    else:
        # extracting mfcc from audio file
        mfcc_result = mfcc(audio_file, frame_length, frame_shift, num_mel_bins)   
        mfcc_list = mfcc_result.tolist()
        mfcc_json = json.dumps(mfcc_list)
    return {'filename': audio_file.filename, 'mfcc': mfcc_json}
